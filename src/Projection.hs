module Projection
  ( project,
    projectRadial,
    projectRadialJitter,
  )
where

import Control.Monad.Random (MonadRandom (getRandomR))
import Linear hiding (project)
import RayM (Ray (Ray))

project :: Int -> Int -> Int -> Int -> Ray
project w h x y =
  let m = (0.5 *) . fromIntegral $ min w h
   in Ray (V3 (((fromIntegral (x - (w `div` 2)))) / m) (((fromIntegral (y - (h `div` 2)))) / m) (-10)) (V3 0 0 1)

projectRadial :: Int -> Int -> Int -> Int -> Ray
projectRadial w h x y =
  let m = (0.5 *) . fromIntegral $ min w h
   in Ray (V3 0 0 (-3.6)) (normalize $ V3 (((fromIntegral (x - (w `div` 2)))) / m) (((fromIntegral (y - (h `div` 2)))) / m) 3)

projectRadialJitter :: MonadRandom m => Int -> Int -> Int -> Int -> m Ray
projectRadialJitter w h x y =
  let Ray origin d1 = projectRadial w h x y
      Ray _ d2 = projectRadial w h (x + 1) (y + 1)
      randomPoint = getRandomR (d1, d2)
   in Ray origin <$> randomPoint