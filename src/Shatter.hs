module Shatter (shatter) where

import Control.Monad.Random
import Linear hiding (rotate)
import RayM

quaternionBetween :: V3 Float -> V3 Float -> Quaternion Float
quaternionBetween src dst =
  let axis = src `cross` dst
      angle = acos $ src `dot` dst
   in axisAngle axis angle

shatter :: MonadRandom m => Float -> Float -> Solid -> m Solid
shatter recurseP explosionD s = do
  d <- randomUnitVector
  let rotation = quaternionBetween (V3 0 0 1) d
  let topMask = rotate rotation $ scale 2 $ translate (V3 0 0 1) solidCube
  let bottomMask = rotate rotation $ scale 2 $ translate (V3 0 0 (-1)) solidCube
  let dp = d ^* explosionD
  let top = translate dp $ topMask `intersection` s
  let bottom = translate (negate dp) $ s `intersection` bottomMask
  rt <- (< recurseP) <$> getRandomR (0, 1)
  rb <- (< recurseP) <$> getRandomR (0, 1)
  rtop <-
    if rt
      then shatter recurseP explosionD top
      else return top
  rbottom <-
    if rb
      then shatter (recurseP * recurseP) explosionD bottom
      else return bottom
  return $ rtop `union` rbottom