module Lib
  ( someFunc,
  )
where

import Codec.Picture
import Control.Lens hiding (transform)
import Control.Monad.Random
import qualified Data.ByteString as BS
import Data.Coerce
import Data.Functor.Identity
import Data.Maybe (fromMaybe, listToMaybe)
import qualified Data.Vector.Unboxed as Vector
import Egg
import Linear hiding (project, rotate)
import Projection (projectRadialJitter)
import RayM
import SaveImage (generateImageM, loadEnvironmentTexture, simpleGamma)
import Shatter

material :: RandomGen g => Colour -> Shader (RayM g)
material c = (diffuseShader c) <> (specularShader 0.05 (grey 0.1))

logLines :: Int -> Int -> IO a -> IO a
logLines 0 y = (>>) (print y)
logLines _ _ = id

someFunc :: IO ()
someFunc = do
  let sphere = scale 0.5 solidSphere
  let span = [-0.5, 0.5]
  ---let spheres = mconcat [shade (material colour) $ translate (V3 x y z) sphere | x <- span, y <- span, (z, colour) <- zip span [green 0.75, red 0.75]]
  ---let bigCube = shade (material $ blue 0.75) $ scale 0.7 solidCube
  -- let shape = rotate (axisAngle (V3 1 0 0) 0.75) $ rotate (axisAngle (V3 0 0 1) 0.75) $ intersection spheres bigCube

  let theEgg = scale 0.8 $ egg 0.8 1.3
  shatteredEgg <- evalRandIO $ shatter 0.9 0.1 $ theEgg `difference` (scale 0.975 theEgg)
  let shape =
        shade (material $ grey 0.75) $
          rotate (axisAngle (V3 0 1 0) (- pi / 2)) $
            theEgg -- shatteredEgg
  let w = 250
  let h = 250
  let samples = 25
  background <- loadEnvironmentTexture "rooitou_park.hdr"
  let env =
        emptyEnvironment
          & addLight ((translate (V3 (-10) 10 (-10)) <$> sphericalLight white))
          & environmentScene .~ shape
          & environmentDepth .~ 2
          & environmentBackground .~ (pure <$> background . (rotate (axisAngle (V3 0 1 0) (- pi / 2))))
          & environmentAmbient .~ pure (grey 0.01)

  let imfn x y = (fmap colourAsVector) $ logLines x y $ sample samples $ (fmap (clampColourMax 2)) $ evalRayIO env (projectRadialJitter w h x y >>= render)
  print "sup"
  img <- ImageRGB8 <$> (generateImageM (simpleGamma . Colour) imfn w h)
  savePngImage "output.png" img
  print "done"
