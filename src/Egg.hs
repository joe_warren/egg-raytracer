module Egg(egg) where
import RayM
import Linear
import Control.Lens
import           Control.Monad                  ( join )

withinEgg :: Float -> Float -> V3 Float -> Bool
withinEgg t lambda p = 
    let 
        r = norm $ p^._xy
        z = p^._z 
        f = t * ((1 + z) ** (1/(1 + lambda))) * ((1 - z)**(lambda/(1 + lambda)))
    in f > r

binarySearchEdge :: Float -> (V3 Float -> Bool) -> V3 Float -> V3 Float -> V3 Float
binarySearchEdge errorTol f near far = 
    let mid = lerp 0.5 near far in
        if errorTol > distance near far 
            then mid
            else 
                if f near == f mid
                    then binarySearchEdge errorTol f mid far
                    else binarySearchEdge errorTol f near mid


findPointInBounds :: Float -> Float -> V3 Float -> V3 Float -> [V3 Float]
findPointInBounds t lambda near far = let
    n = 20
    step = 1/(fromIntegral n)
    errorTol = 1e-5
    lerp' f = lerp f far near
    points = lerp' <$> iterate (+step) 0
    ranges = take n $ zip points (tail points)
    f = withinEgg t lambda
    filterFn (first, second) = f first /= f second
    interestingRanges = filter filterFn ranges
    in
        (uncurry $ binarySearchEdge errorTol f) <$> interestingRanges
        
addNormal :: Float -> Float -> V3 Float -> Collision
addNormal t lambda p = Collision p n
    where 
        z = p^._z
        f' =  -(t * (lambda * z + lambda + z - 1) * ((1 - z)**((-1)/(1 + lambda))) * ((1 + z)**(1/(1 + lambda) - 1)))/(1 + lambda)
        nz = -(signum f') *  sqrt (f' * f' / (f' * f' + 1))
        nr = sqrt (1 - nz*nz)
        xy = nr *^ (normalize $ p^._xy)
        n = normalize $ zero & _z .~ nz & _xy .~ xy 

egg :: Float -> Float -> Solid
egg t lambda = Solid rayFn
  where
    rayFn ray = 
        let 
            -- adding a little extra to the bounds lets us play slightly faster and looser with some of the maths above
            boundScale = V3 (t*1.01) (t*1.01) 1.01
            boundingCylinder = scale3d boundScale solidCylinder 
            bounds = solidCollision boundingCylinder ray
            in
                case bounds of 
                    [near, far] -> addNormal t lambda <$> findPointInBounds t lambda (near^.collisionPoint) (far^.collisionPoint)
                    _ -> []