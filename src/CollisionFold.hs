module CollisionFold (collisionFold, joinEither) where

import Data.Foldable (foldl')

type Acc a = ((Bool, Bool), [a])

flipLR :: (Bool, Bool) -> Either a b -> (Bool, Bool)
flipLR (l, r) (Left _) = (not l, r)
flipLR (l, r) (Right _) = (l, not r)

joinEither :: Either a a -> a
joinEither (Left a) = a
joinEither (Right a) = a

collisionFoldStep :: (Bool -> Bool -> Bool) -> Acc a -> Either a a -> Acc a
collisionFoldStep fn (inLR, xs) eab = let inLR' = flipLR inLR eab in 
    (inLR', if ((uncurry fn) inLR') /= ((uncurry fn) inLR) then joinEither eab : xs else xs)

collisionFold :: (Bool -> Bool -> Bool) -> [Either a a] -> [a] 
collisionFold fn cs = reverse $ snd $ foldl' (collisionFoldStep fn) ((False, False), []) cs