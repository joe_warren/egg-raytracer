module SaveImage
  ( generateImageM,
    simpleGamma,
    loadEnvironmentTexture,
  )
where

import Codec.Picture
import Control.Lens hiding (transform)
import qualified Data.Vector.Unboxed as Vector
import Control.DeepSeq (NFData, force)
import Control.Monad.Primitive
import Linear
import RayM (Colour (Colour), Ray (Ray), rayDirection)

generateImageM :: (Pixel px, PrimMonad m, NFData a) => (a -> px) -> (Int -> Int -> m a) -> Int -> Int -> m (Image px)
generateImageM gamma f w h = withImage w h (\x y -> evalPrim =<< (let m = f y x in m `seq` (\c -> let fc = force c in fc `seq` gamma fc) <$> m))

simpleGamma :: Colour -> PixelRGB8
simpleGamma (Colour (V3 r g b)) =
  let f = floor . (255 *) . (max 0) . (min 1)
   in PixelRGB8 (f r) (f g) (f b)

loadEnvironmentTexture :: String -> IO (Ray -> Colour)
loadEnvironmentTexture path = do
  Right (ImageRGBF image) <- readImage path
  let atPixel = uncurry (pixelAt image)
  let wI = imageWidth image
  let hI = imageHeight image
  let w = fromIntegral $ wI
  let h = fromIntegral $ hI
  let x f = min (wI -1) $ max 0 $ floor $ w * (f + pi) / (2 * pi)
  let y f = min (hI -1) $ max 0 $ floor $ h * (f + pi / 2) / pi
  let rayToI = (\s -> (x $ s ^. _y, y $ s ^. _z)) . vectorToSpherical . (^. rayDirection)
  let pxColour (PixelRGBF r g b) = Colour $ V3 r g b
  return $ pxColour . atPixel . rayToI
  where
    vectorToSpherical :: V3 Float -> V3 Float
    vectorToSpherical v@(V3 x y z) =
      let rho = norm $ (v ^. _xy)
       in V3 (norm v) (atan2 x y) (atan2 z rho)
