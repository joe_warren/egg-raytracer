{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module FurnitureGenerator.Backs (
    Back,
    backs, 
    noBacks
) where 

import Control.Applicative hiding (empty)
import Control.Monad
import Control.Monad.Random
import RayM
import Linear (axisAngle, V3 (V3))

type Back m = Float -> m Solid

backs :: MonadRandom m => [Back m]
backs = [boringBack,roundedBack, horizontalBeamBack, verticalBeamBack]

noBacks :: Monad m => [Back m]
noBacks = [noBack]

noBack :: Monad m => Back m
noBack = const $ return empty

reposeBack :: MonadRandom m => Solid -> m Solid
reposeBack back = do
    repose <- getRandomR (0, pi/8)
    return $ rotate (axisAngle (V3 1.0 0.0 0.0) repose) back

boringBack :: MonadRandom m => Back m
boringBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    reposeBack $ translate (V3 0.0 (-depth/2) (height/2)) $ scale3d (V3 width depth height) halfCube

roundedBack :: MonadRandom m => Back m
roundedBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    curvature <- getRandomR (3.0, 6.0)
    let boxA = translate (V3 0.0 (-depth/2) (height/2)) $ scale3d (V3 (width-curvature*2) depth height) halfCube
    let boxB = translate (V3 0.0 (-depth/2) ((height-curvature)/2)) $ scale3d (V3 width depth (height-curvature)) halfCube
    let cylinder = rotate (axisAngle (V3 1 0 0) (pi/2)) $ scale3d (V3 curvature curvature depth) $ halfCylinder 
    let cylinders = mconcat $ map (\i -> translate (V3 (i*(width/2-curvature)) (-depth/2) (height-curvature)) cylinder) [-1, 1]
    reposeBack $ mconcat [boxA, boxB, cylinders] 

verticalBeamBack :: MonadRandom m => Back m
verticalBeamBack width = do 
    height <- getRandomR (30, 40)
    depth <- getRandomR (0.5, 5.0)
    topHeight <- getRandomR (0.5, 10.0)
    let top = translate (V3 0 0 (height -topHeight/2)) $ scale3d (V3 width depth topHeight) halfCube
    beamCount :: Integer <- getRandomR(3, 8)
    beamR <- getRandomR (0.5, min depth (width-depth)/fromIntegral beamCount)
    let singleBeam = scale3d (V3 beamR  beamR height) halfCube
    let beamPositionsOffset = map (\i -> (width-depth) * fromIntegral i/fromIntegral beamCount) [0..beamCount]
    let beamPositions = map (((width-depth)/2.0) -) beamPositionsOffset
    let beams = map (\p -> translate (V3 p 0 (height/2.0)) singleBeam ) beamPositions
    reposeBack $ mconcat (top:beams) 

horizontalBeamBack :: MonadRandom m => Back m
horizontalBeamBack width = do
    height <- getRandomR (30, 40)
    beamDepth <- getRandomR (3.0, 5.0)
    beamWidth <- getRandomR (beamDepth/4, beamDepth/2)
    let verticalBeam = scale3d (V3 beamWidth beamDepth height) $ translate (V3 0 0 0.5) halfCube
    let verticals = [translate (V3 (i*width/2) 0 0) verticalBeam | i <- [-1, 1]]
    let horizontalBeam = scale3d (V3 width beamWidth beamDepth) halfCube
    hBeamStart <- getRandomR (0, height/2)
    let hBeamRegion = height - hBeamStart
    hBeamCount :: Int <- getRandomR (2, floor $ hBeamRegion/beamDepth)
    let hBeams = [translate (V3 0 0 (hBeamStart + (fromIntegral i-0.5)*hBeamRegion/fromIntegral hBeamCount)) horizontalBeam | i <- [1..hBeamCount]]
    reposeBack $ mconcat  $ verticals ++ hBeams


