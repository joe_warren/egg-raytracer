
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module FurnitureGenerator.Objects (
 chair, 
 bench, 
 table, 
 randomTable,
 randomItem,
 randomArrangement,
 randomItems
) where

import Data.Maybe
import Control.Applicative
import Control.Monad
import Control.Monad.Random
import FurnitureGenerator.Choice
import qualified FurnitureGenerator.Arms as Arms
import qualified FurnitureGenerator.Legs as Legs
import qualified FurnitureGenerator.Seats as Seats
import qualified FurnitureGenerator.Backs as Backs
import RayM
import Linear (V3(V3), axisAngle)

chair :: MonadRandom m => m Solid
chair = abstractObject (getRandomR (20, 30)) (getRandomR (20, 30)) Legs.legs Seats.seats Backs.backs Arms.arms

bench :: MonadRandom m => m Solid
bench = abstractObject (getRandomR (60, 90)) (getRandomR (20, 30)) Legs.benchLegs Seats.benchSeats Backs.backs Arms.arms

table :: MonadRandom m => Int -> Int -> m Solid
table width depth = do
    unitSize <- getRandomR (45,55)
    let w = unitSize * fromIntegral width
    let d = unitSize * fromIntegral depth
    case width == depth of
      True -> abstractObject (return w) (return d) Legs.symetricTableLegs Seats.symetricTableTops Backs.noBacks Arms.noArms
      False -> abstractObject (return w) (return d) Legs.tableLegs Seats.tableTops Backs.noBacks Arms.noArms

randomTable :: MonadRandom m => m Solid
randomTable = do
    w <- getRandomR (1, 3)
    h <- getRandomR (1, 3)
    table w h

randomItem :: MonadRandom m => m Solid
randomItem = join $ choice [randomTable, chair, bench]

data SideOption = EmptySide | ChairSide | BenchSide 

sideOptionChoice :: MonadRandom m => Int -> m SideOption
sideOptionChoice 0 = return EmptySide
sideOptionChoice 1 = choice [EmptySide, ChairSide]
sideOptionChoice _ = choice [EmptySide, ChairSide, BenchSide]  

untupleMaybe :: (a, Maybe b) -> Maybe (a, b)
untupleMaybe (a, (Just b)) = Just (a, b)
untupleMaybe (a, Nothing) = Nothing

reifySide :: MonadRandom m => SideOption -> Int -> Float -> Solid -> Solid -> m [Solid]
reifySide EmptySide _ _ _ _ = return []
reifySide BenchSide _ _ _ b = return [b]
reifySide ChairSide n len c _ = do
    let possiblyChair = do
                          b <- getRandom 
                          return $ case b of
                                    True -> Just c
                                    False -> Nothing
    unpositionedChairMaybes <- replicateM n possiblyChair
    let positions = [((0.5 + fromIntegral i)*len/(fromIntegral n)) - (len/2) | i <- [0..n-1]]

    let positionChairs = catMaybes $ untupleMaybe <$> zip positions unpositionedChairMaybes 
   
    return $ (\(p, c) -> translate (V3 p 0 0) c) <$> positionChairs

-- TODO: This is one of those bits that doesn't work
-- because the model we're using for solids doesn't let you calculate a bounding box
level :: Solid -> Solid
level o = translate (V3 0 0 (-h)) o
  where 
      h = 1
randomArrangement :: MonadRandom m => m [Solid]
randomArrangement = do
    w <- getRandomR (1, 3)
    d <- getRandomR (1, 3)
    let lengths = [w, d, w, d]
    sides <- sequence $ sideOptionChoice <$> lengths
    theChair <- chair
    theBench <- bench
    table <- table w d
    let (wd, dd) = (\((x1, y1, z1), (x2, y2, z2)) -> (x1-x2, y1-y2)) $ ((1, 1, 1), (0, 0, 0)) -- Csg.aabb table
    let lenD  = [wd, dd, wd, dd]
    sides <- sequence $ (( uncurry $ uncurry reifySide) <$> (zip (zip sides lengths) lenD)) <*> [theChair] <*> [theBench]
    let tr = [(dd/2, 0), (wd/2, pi/2), (dd/2, pi), (wd/2, 3*pi/2)]
    let backOff = 0.5 * wd/fromIntegral w
    let positionSide = (\(s, (t, a)) ->((rotate (axisAngle (V3 0 0 1) a)).(translate (V3 0 (t+backOff) 0))) <$> s)
    let positionedSides = positionSide <$> (zip sides tr)
    return $ level <$> (table : (concat positionedSides))

randomItems :: MonadRandom m => m [Solid]
randomItems = do
    b <- getRandom
    case b of
        True -> randomArrangement
        False -> (:[]) <$> chair

abstractObject :: MonadRandom m => m Float -> m Float -> [Legs.Legs m] -> [Seats.Seat m] -> [Backs.Back m] -> [Arms.Arm m] -> m Solid
abstractObject widthR depthR legFns seatFns backFns armFns = do 
    width <- widthR
    depth <- depthR
    legsFn <- choice legFns 
    legs <- legsFn width depth
    seatFn <- choice seatFns
    seat <- seatFn width depth
    backFn <- choice backFns
    back <- backFn width
    armFn <- choice armFns
    arm <- armFn depth
    let bothArms = Arms.pluraliseArm width arm
    let backPositioned = translate (V3 0.0 (-depth/2) 0.0) back 
    return $ legs <> seat <> backPositioned <> bothArms


