{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module FurnitureGenerator.Arms (arms, noArms, pluraliseArm, Arm) where

import Control.Monad.Random
import Linear (axisAngle)
import Linear.V3
import RayM

type Arm m = Float -> m Solid

arms :: MonadRandom m => [Arm m]
arms = [noArm, boringArm, foldingArm, fancyArm]

noArms :: MonadRandom m => [Arm m]
noArms = [noArm]

boringArm :: MonadRandom m => Arm m
boringArm depth = do
  height <- getRandomR (8.0, 16.0)
  width <- getRandomR (3.0, 6.0)
  armDepth <- getRandomR (depth / 2, depth)
  armrestHeight <- getRandomR (width / 3, width)
  let armrest = translate (V3 0 0 height) $ scale3d (V3 width armDepth armrestHeight) halfCube
  let armbeam = translate (V3 0 0 height / 2.0) $ scale3d (V3 width width height) halfCube
  return $ armrest `union` armbeam

fancyArm :: MonadRandom m => Arm m
fancyArm depth = do
  height <- getRandomR (8.0, 16.0)
  width <- getRandomR (3.0, 6.0)
  radius <- getRandomR (width / 2.0, width)
  beamWidth <- getRandomR (width / 5.0, width / 2.0)
  armDepth <- getRandomR (depth / 2, depth)
  armrestHeight <- getRandomR (width / 3, width)
  let armrest = translate (V3 0 0 height) $ scale3d (V3 width armDepth armrestHeight) halfCube
  let armbeam = translate (V3 0 0 height / 2.0) $ scale3d (V3 beamWidth beamWidth height) halfCube
  numBeams :: Integer <- getRandomR (1, 4)
  let beamPositions = map (\i -> 0.8 * armDepth * ((fromIntegral i / fromIntegral numBeams) - 0.5)) [0 .. numBeams]
  let beams = map (\p -> translate (V3 0.0 p 0.0) armbeam) beamPositions
  let cylinder = translate (V3 (radius - width / 2.0) (armDepth / 2) height) $ scale3d (V3 radius radius armrestHeight) $ halfCylinder
  return $ mconcat $ armrest : cylinder : beams

foldingArm :: MonadRandom m => Arm m
foldingArm depth = do
  height <- getRandomR (8.0, 16.0)
  width <- getRandomR (3.0, 6.0)
  armDepth <- getRandomR (depth / 2, depth)
  armrestHeight <- getRandomR (width / 3, width)
  beamWidth <- getRandomR (width / 6, width / 2)
  let beamHeight = sqrt $ height * height + armDepth * armDepth
  let armrest = translate (V3 0 0 height) $ scale3d (V3 width armDepth armrestHeight) halfCube
  let beamDepth = armDepth - beamWidth
  let beamHeight = sqrt $ beamDepth * beamDepth + height * height
  let armbeam = scale3d (V3 beamWidth beamWidth beamHeight) halfCube
  let angle = atan $ beamDepth / height
  let beam1 = translate (V3 (beamWidth / 2) 0 (height / 2)) $ rotate (axisAngle (V3 1 0 0) angle) armbeam
  let beam2 = translate (V3 (- beamWidth / 2) 0 (height / 2)) $ rotate (axisAngle (V3 (-1) 0 0) angle) armbeam
  return $ armrest `union` beam1 `union` beam2

noArm :: MonadRandom m => Arm m
noArm = const $ return empty

pluraliseArm :: Float -> Solid -> Solid
pluraliseArm width arm = rightArm `union` leftArm
  where
    rightArm = translate (V3 (width / 2) 0 0) arm
    leftArm = scale3d (V3 (-1) 1 1) rightArm
