module FurnitureGenerator.Choice
    ( choice
    ) where

import Control.Monad
import Control.Monad.Random

-- I don't like using unsafe indexing here, but it works
-- And if we're being honnest, I had no choice
choice :: MonadRandom m => [a] -> m a 
choice xs = (xs !!) <$> getRandomR (0, length xs - 1)

