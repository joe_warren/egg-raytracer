
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module FurnitureGenerator.Seats (
    Seat,
    seats,
    benchSeats, 
    tableTops, 
    symetricTableTops
) where

import Control.Applicative
import Control.Monad
import Control.Monad.Random ( MonadRandom(getRandomR) )
import RayM
import Linear (V3(V3))

type Seat m =  Float -> Float -> m Solid

seats :: MonadRandom m => [Seat m]
seats = [boringSeat, roundedSeat, circularSeat]

benchSeats :: MonadRandom m => [Seat m]
benchSeats = [boringSeat, roundedSeat]

tableTops :: MonadRandom m => [Seat m]
tableTops = [boringSeat, roundedSeat]

symetricTableTops :: MonadRandom m => [Seat m]
symetricTableTops = [boringSeat, roundedSeat, circularSeat]

boringSeat :: MonadRandom m => Seat m
boringSeat width depth = do 
    height <- getRandomR (0.5, 5.0)
    padding <- getRandomR (3.0, 6.0)
    return $ scale3d (V3 (width+padding) (depth+padding) height) halfCube

roundedSeat :: MonadRandom m => Seat m
roundedSeat width depth = do
    height <- getRandomR (0.5, 5.0)
    curvature <- getRandomR (3.0, 6.0)
    let boxA = scale3d (V3 width (depth+curvature*2.0) height) halfCube
    let boxB = scale3d (V3 (width + curvature*2.0) depth height) halfCube
    let cylinder = scale3d (V3 curvature curvature height) $ halfCylinder
    let positions = [V3 (i*width) (j*depth) 0.0 | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let cylinderList = map (`translate` cylinder) positions
    let corners = mconcat  cylinderList
    return $ boxA <> boxB  <> corners

circularSeat :: MonadRandom m => Seat m
circularSeat width depth = do
    height <- getRandomR (0.5, 5.0)
    padding <- getRandomR (3.0, 5.0)
    let r = padding + sqrt (width*width + depth*depth) /2.0
    return $ scale3d (V3 r r height) $ halfCylinder 


