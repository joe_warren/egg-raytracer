{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RankNTypes #-}
module FurnitureGenerator.Legs (
    Legs,
    legs, 
    benchLegs,
    tableLegs,
    symetricTableLegs
) where

import Control.Applicative
import Control.Monad
import Control.Monad.Random
import RayM
import Linear (V3(V3), axisAngle)

type Legs m = Float -> Float -> m Solid

legs :: MonadRandom m => [Legs m]
legs = [boringLegs, midBarLegs, swivelChairLegsWithCastors, swivelChairLegsNoCastors, foldingLegs, archLegs]

benchLegs :: MonadRandom m => [Legs m]
benchLegs =  [boringLegs, midBarLegs, foldingLegs]

tableLegs :: MonadRandom m => [Legs m]
tableLegs =  [midBarLegs, foldingLegs, archLegs]

symetricTableLegs :: MonadRandom m => [Legs m]
symetricTableLegs = [midBarLegs, swivelChairLegsWithCastors, swivelChairLegsNoCastors, foldingLegs, archLegs]

boringLegs :: MonadRandom m => Legs m
boringLegs width depth = do
    height <- getRandomR (20.0, 40.0)
    legW <- getRandomR (0.5, 2.0)
    legD <- getRandomR (legW, 3.0)
    let leg = scale3d (V3 legW legD height) halfCube 
    let positions = [(V3 (i*width) (j*depth) 0.0) | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let legList = map (`translate` leg) positions
    let legs =  mconcat legList
    beamHeight <- getRandomR(-height/4.0, height/4.0)
    let beam = translate (V3 0.0 (-depth/2.0) beamHeight) $ scale3d (V3 width legW legD) halfCube

    return $ translate (V3 0.0 0.0 (-height/2.0)) $ legs `union` beam

midBarLegs :: MonadRandom m => Legs m
midBarLegs width depth = do
    height <- getRandomR (20.0, 40.0)
    legW <- getRandomR (0.5, 2.0)
    legD <- getRandomR (legW, 3.0)
    let leg = scale3d (V3 legD legW height) halfCube 
    let positions = [V3 (i*width) (j*depth) 0.0 | i <- [-0.5, 0.5], j<- [-0.5, 0.5]]
    let legs = map (`translate` leg) positions
    beamHeight <- getRandomR(-height/4.0, height/4.0)
    let beams = [translate (V3 (i*width) 0 beamHeight) $ scale3d (V3 legW depth legD) halfCube | i <-[-0.5, 0.5]]

    let crossBeam = translate (V3 0 0 beamHeight) $ scale3d (V3 width legD legW) halfCube 

    return $ translate (V3 0.0 0.0 (-height/2.0)) $ mconcat (crossBeam : legs ++ beams)

swivelChairLegs :: MonadRandom m => Bool -> Legs m
swivelChairLegs includeCastors width depth = do
    height <- getRandomR (25.0, 50.0)
    centerR1 <- getRandomR (1.5, 5.0)
    centerR2 <- getRandomR (1.2, centerR1)
    heightFeet <- getRandomR (1.0, 10.0)
    heightR1 <- getRandomR (5.0, (height -heightFeet)/2.0)
    let heightBeam = height - heightFeet
    let beamR2 = translate (V3 0.0 0.0 (-heightBeam/2)) $ scale3d (V3 centerR2 centerR2 heightBeam) halfCylinder
    let beamR1 = translate (V3 0.0 0.0 (heightR1/2-heightBeam)) $ scale3d (V3 centerR1 centerR1 heightR1) halfCylinder
    let beam = beamR1 <> beamR2

    numFeet ::Int <- getRandomR(3, 8)
    let rotations = [(2.0 * pi * fromIntegral i)/fromIntegral numFeet | i <- [0..numFeet-1]]

    footPadding <- getRandomR (3.0, 6.0)
    let footLengthY = (max width depth) /2 + footPadding
    let footLength = sqrt ( footLengthY * footLengthY + heightFeet * heightFeet) 
    let footAngle = atan (heightFeet/footLengthY)
    footWidth <- getRandomR (1.5, centerR1)
    let footBeam = rotate (axisAngle (V3 (-1.0) 0.0 0.0) footAngle) $ translate (V3 0.0 (footLength/2.0) (footWidth/2.0)) $ scale3d (V3 footWidth footLength footWidth) halfCube
    let footBeamList = map (\r -> rotate (axisAngle (V3 0.0 0.0 1.0) r) footBeam) rotations
    let foot = translate (V3 0.0 0.0 (heightFeet-height)) $ mconcat footBeamList
    legOffsetRotation <- getRandomR (0, pi)
    includeBaseLegs <- case includeCastors of 
                    True -> return True
                    False -> getRandom
    baseLegHeight <- getRandomR (5, 10)
    let baseLegOffset = V3 
                            0
                            ((footLength*cos footAngle) + (footWidth*sin footAngle) - (footWidth/2)) 
                            ((-footLength*sin footAngle) + (footWidth*cos footAngle) - baseLegHeight/2 - height + heightFeet)
    baseLegs <- case includeBaseLegs of
                True -> do
                    let baseBeam = translate baseLegOffset $ scale3d (V3 footWidth footWidth baseLegHeight) halfCube
                    let baseBeamList = map (\r -> rotate (axisAngle (V3 0.0 0.0 1.0) r) baseBeam) rotations
                    return $ mconcat baseBeamList
                False -> return mempty
    castors <- case includeCastors of
                True -> do
                    castorRadius <- getRandomR (3, 6)
                    castorWidth <- getRandomR (footWidth * sqrt 2, footWidth*2.5)
                    let castor = translate (V3 (castorRadius - (footWidth/sqrt 2)) 0 0) $ rotate (axisAngle (V3 1 0 0) (pi/2)) $ scale3d (V3 castorRadius castorRadius castorWidth) $ halfCylinder
                    let rotatedCastors = (\a -> rotate (axisAngle (V3 0 0 1) a) $ translate (V3 0 0 (-baseLegHeight/2)) $ translate baseLegOffset $ rotate (axisAngle (V3 0 0 (-1)) a) castor) <$> rotations
                    return $ mconcat rotatedCastors
                False -> return mempty
    let footPlusBase = foot <> baseLegs <> castors
    let rotatedFoot = rotate (axisAngle (V3 0 0 1) legOffsetRotation) footPlusBase 

    return $ beam <> rotatedFoot

swivelChairLegsWithCastors :: MonadRandom m => Legs m
swivelChairLegsWithCastors = swivelChairLegs True
swivelChairLegsNoCastors :: MonadRandom m => Legs m
swivelChairLegsNoCastors = swivelChairLegs False
archLegs :: MonadRandom m => Legs m
archLegs width depth = do
    heightPlus :: Float <- getRandomR (2.0, 8.0)
    let height :: Float = min 60 $ max width (depth/2) + heightPlus
    footSize <- getRandomR (2.0, 6.0)
    let widthR = (width/2.0) - footSize
    let depthR = (depth/2.0) - footSize
    let widthCyl = rotate (axisAngle (V3 1 0 0) (pi/2)) $ scale3d (V3 widthR widthR (depth+10.0)) $ halfCylinder 
    let depthCyl = rotate (axisAngle (V3 0 1 0) (pi/2)) $ scale3d (V3 depthR depthR (width+10.0)) $ halfCylinder 
    let bothCyls = translate (V3 0 0 (-height)) (widthCyl <> depthCyl)
    let cube = translate (V3 0 0 (-height/2)) $ scale3d (V3 width depth height) halfCube
    return $ cube `difference` bothCyls

foldingLegs :: MonadRandom m => Legs m
foldingLegs width depth = do
    height <- getRandomR (20, 40)
    barWidth <- getRandomR (0.5, 2.0)
    let barLength = sqrt (depth * depth + height * height)
    let bar = scale3d (V3 width barWidth barLength) halfCube `difference` scale3d (V3 (width-2*barWidth) (barWidth*2) (barLength -2*barWidth)) halfCube
    let angle = atan (depth/height)
    let part1 = translate (V3 (barWidth/2) 0  (-height/2)) $ rotate (axisAngle (V3 1 0 0) angle) bar
    let part2 = translate (V3 (-barWidth/2) 0 (-height/2)) $ rotate (axisAngle (V3 (-1) 0 0) angle) bar
    return $ part1 <> part2


