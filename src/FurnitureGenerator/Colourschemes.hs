
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ScopedTypeVariables #-}
module FurnitureGenerator.Colourschemes( 
    HSL (..), 
    rgbToHsl,
    hslToRgb, 
    lighter,
    darker,
    rotate,
    randomColor,
    randomColors,
    saveRandomPallet
    ) where

import Control.Applicative
import Control.Monad
import Control.Monad.Random
import Data.Fixed (mod')
import FurnitureGenerator.Choice
import qualified Codec.Picture as P
import Linear (V3(V3))
import RayM (Colour(Colour))

newtype HSL = HSL { hsl :: V3 Float }  deriving Show

rgbToHsl :: Colour -> HSL
rgbToHsl (Colour (V3 r g b)) = HSL $ V3 h s l
  where
    ma = maximum [r, g, b]
    mi = minimum [r, g, b]
    range = ma - mi
    h = case (ma==r, ma==g, ma==b) of
        (True, _, _) -> (pi/3) * (g-b)/range
        (_, True, _) -> (pi/3) * (2 + ((b-r)/range))
        (_, _, True) -> (pi/3) * (4 + ((r-g)/range))
        (_, _, _) -> 0
    l = (ma + mi)/2
    sd = if (ma + mi) < 1 then (ma + mi) else (2 - (ma+mi))
    s = if (ma == mi) then 0 else (ma - mi) / sd

hslToRgb :: HSL -> Colour
hslToRgb (HSL (V3 h s l)) = Colour $ V3 (bound r) (bound g) (bound b)
  where
    h' = (h*3/pi) `mod'` 6
    c = s * (1 - abs ((2 * l) - 1))
    x = c * (1 - abs ((h' `mod'` 2) - 1))
    (r', g', b') = case floor h' of 
                 0 -> (c, x, 0)
                 1 -> (x, c, 0)
                 2 -> (0, c, x)
                 3 -> (0, x, c)
                 4 -> (x, 0, c)
                 5 -> (c, 0, x)
                 _ -> (0, 0, 0)
    m = l - (c/2)
    bound = (max 0).(min 1)
    (r, g, b) = (r' + m, g' + m, b' + m)

lighter :: Float -> Colour -> Colour
lighter a = hslToRgb . (\(HSL (V3 h s l)) -> HSL $ V3 h s (min 1 (l+a))) . rgbToHsl

darker :: Float -> Colour -> Colour
darker a = hslToRgb . (\(HSL (V3 h s l)) -> HSL $ V3 h (lower s) (lower l)) . rgbToHsl
  where
    lower = (max 0).(flip (-) a)

rotate :: Float -> Colour -> Colour
rotate a = hslToRgb . (\(HSL (V3 h s l)) -> HSL $ V3 ((h+a)`mod'`(2*pi)) s l) . rgbToHsl

randomColor :: MonadRandom m => m Colour
randomColor = hslToRgb <$> (\h -> HSL $ V3 h 1 0.5) <$> getRandomR (0, 2*pi)

randomColors' :: MonadRandom m => Int -> m [Colour]
randomColors' 2 = do
    angle <- choice [pi/6, pi/4, pi/3, pi/2, pi]
    c <- randomColor
    return [c, rotate angle c]
randomColors' 3 = do
    angle <- choice [pi/6, pi/4, pi/3, pi/2, 2*pi/3]
    c <- randomColor
    return [c, rotate angle c, rotate (angle*2) c]
randomColors' i = do
    let fi = fromIntegral i
    strat <- getRandom
    case strat of
      True -> do
                angle <- getRandomR (0.25*pi/fi, 2*pi/fi)
                c <- randomColor
                return $ take i $ iterate ((rotate angle)) c
      False -> do
                v <- randomColors' 3
                let [c1, c2, c3] = v
                return $ c1:c2:(replicate (i-2) c3)


randomColors :: MonadRandom m => Int -> m [Colour]
randomColors i = do
    g <- getRandomR (0, 1)
    bw <- choice [Colour 0, Colour 1, Colour $ V3 g g g]
    c <- choice [ 
        randomColors' i,
        (bw :) <$> randomColors' (i - 1) 
      ]
    c
-- the following methods are just useful for debugging
-- and do not make up a part of the application
generatePalletImage :: [Colour] -> P.DynamicImage
generatePalletImage colors = P.ImageRGB8 img
  where
    l = length colors
    img = P.generateImage imgFn 200 200
    s = fromIntegral . floor . (* 0xFF)
    toPx (Colour (V3 r g b)) = P.PixelRGB8 (s r) (s g) (s b)
    cFn w h = colors !! ((((w*l) `quot` 200) + ((h*l) `quot` 200)) `mod` l)
    imgFn w h = toPx (cFn w h)

saveRandomPallet :: Int -> IO ()
saveRandomPallet i = do
    pallet <- evalRandIO $ randomColors i
    P.savePngImage "pallet.png" $ generatePalletImage pallet
