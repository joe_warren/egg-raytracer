module FurnitureGenerator.Lib (someFunc) where

import Codec.Picture
import Control.Lens hiding (transform)
import Control.Monad.Random
import qualified Data.ByteString as BS
import Data.Coerce
import Data.Functor.Identity
import Data.Maybe (fromMaybe, listToMaybe)
import qualified Data.Vector.Unboxed as Vector
import Egg
import Linear hiding (project, rotate)
import Projection (projectRadialJitter)
import RayM
import SaveImage (generateImageM, loadEnvironmentTexture, simpleGamma)
import qualified FurnitureGenerator.Objects as Objects
import qualified FurnitureGenerator.Colourschemes as Colourschemes

material :: RandomGen g => Colour -> Shader (RayM g)
material c = (diffuseShader c) <> (specularShader 0.05 (grey 0.1))

logLines :: Int -> Int -> IO a -> IO a
logLines x 0 = (>>) (print x)
logLines _ _ = id

someFunc :: IO ()
someFunc = do
  chair <- evalRandIO $ Objects.chair
  colour <- evalRandIO $ Colourschemes.randomColor
  let shape =
        shade (material $ colour) $
          rotate (axisAngle (V3 0 1 0) (- pi / 8)) $
          rotate (axisAngle (V3 1 0 0) (- pi / 4)) $
          rotate (axisAngle (V3 0 1 0) (- pi / 2)) $
            scale 0.025
            chair -- shatteredEgg
  let w = 600
  let h = 600
  let samples = 50
  background <- loadEnvironmentTexture "kiara.hdr"
  let env =
        emptyEnvironment
          & addLight ((translate (V3 (-10) 10 (-10)) <$> sphericalLight white))
          & environmentScene .~ shape
          & environmentDepth .~ 2
          & environmentBackground .~ (pure <$> background . (rotate (axisAngle (V3 0 1 0) (- pi / 2))))
          & environmentAmbient .~ pure (grey 0.01)

  let imfn x y = (fmap colourAsVector) $ logLines x y $ sample samples $ (fmap (clampColourMax 2)) $ evalRayIO env (projectRadialJitter w h x y >>= render)
  print "sup"
  img <- ImageRGB8 <$> (generateImageM (simpleGamma . Colour) imfn w h)
  savePngImage "chair.png" img
  print "done"
